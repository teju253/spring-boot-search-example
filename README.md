### Search Engine

This project provides UI functionality to search books

Built With

- SpringBoot - An enabler to create stand-alone production-grade Spring based applications.
- SpringMVC - The Web/MVC framework that creates beautiful websites
- Thymeleaf - The templating engine to render wonderful HTML
- Bootstrap - The framework that makes usability, as easy as ABC
- Maven - Dependency & Build Management


Books Search Steps

- Run the application and access it on browser at "http://localhost:8080/?search=comics"
- Search for Comics, cartoon or Kindle and enter
- Search Results appear, according to the search keyword entered
- If given keyword for the books are not found, then you see error div.
- Try again and search for the list provided above.


Java Coding Standards

- Avoid developing too long classes
- Avoid multiple return statements
- Exceptions- catch the exception, and add the proper error message.
- Considering setters and getters for field access
- Structure code correctly
- Avoid Using default package-Make sure that everything lives in a well-named package.
- Keep Application.java in the top-level source directory
- Keep @controller clean and focused.
- Building services around business capabilities






