package com.search.controller;

import com.search.model.ItemDetails;
import com.search.service.HibernateSearchService;
import com.search.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/*This is controller class responsible for connecting
the backend service to front end Thymeleaf template.*/
@Controller
public class SearchController {

    @Autowired
    private HibernateSearchService searchservice;

    @Autowired
    private SearchService searchService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String search(@RequestParam(value = "search", required = false) String q, Model model) {
        List<ItemDetails> searchResults = null;
        try {
            searchService.addItems();
            searchResults = searchservice.fuzzySearch(q);

        } catch (Exception ex) {
            ex.getMessage();
            System.out.println("Item entered not found"+ex.getMessage());
            // ...
            // throw ex;
        }
        model.addAttribute("search", searchResults);
        return "index";

    }

}
