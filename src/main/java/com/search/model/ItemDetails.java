package com.search.model;

import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Indexed
@Entity
public class ItemDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long itemId;
    @Field
    private String itemName;
    @Field
    private String itemRating;
    @Field
    private int customerReviews;


    public long getItemId() {
        return itemId;
    }

    public void setItemId(long itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemRating() {
        return itemRating;
    }

    public void setItemRating(String itemRating) {
        this.itemRating = itemRating;
    }

    public int getCustomerReviews() {
        return customerReviews;
    }

    public void setCustomerReviews(int customerReviews) {
        this.customerReviews = customerReviews;
    }

}
