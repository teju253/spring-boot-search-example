package com.search.service;

import com.search.dao.BooksRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.search.model.ItemDetails;

@Service
public class SearchServiceImpl implements SearchService {

    @Autowired
    BooksRepository booksRepository;

    ItemDetails firstItem = new ItemDetails ();
    ItemDetails  secondItem = new ItemDetails ();
    ItemDetails  thirdItem = new ItemDetails ();

    public void addItems() {
        firstItem.setItemName("comics");
        firstItem.setItemRating("Good");
        firstItem.setCustomerReviews(2);

        booksRepository.save(firstItem);


        secondItem.setItemName("cartoon");
        secondItem.setItemRating("Good");
        secondItem.setCustomerReviews(3);

        booksRepository.save(secondItem);

        thirdItem.setItemName("kindle");
        thirdItem.setItemRating("Fair");
        thirdItem.setCustomerReviews(4);

        booksRepository.save(thirdItem);

        System.out.println("Items have been added : " + booksRepository.findAll());

    }
}
