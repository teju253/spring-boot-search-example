package com.search.dao;

import org.springframework.data.repository.CrudRepository;

import com.search.model.ItemDetails ;

/*CrudRepository for the ItemDetails model that will
allow us to perform the create and read functions needed.*/

public interface BooksRepository extends CrudRepository<ItemDetails ,Long> {

}
